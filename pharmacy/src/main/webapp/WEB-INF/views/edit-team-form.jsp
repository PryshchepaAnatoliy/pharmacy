<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
    <title>Аптека-Онлайн</title>

    <!-- Bootstrap core CSS -->
  	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/carousel.css" rel="stylesheet">
  <style>
    .form    {
background: -webkit-gradient(linear, bottom, left 175px, from(#CCCCCC), to(#EEEEEE));
background: -moz-linear-gradient(bottom, #CCCCCC, #EEEEEE 175px);
margin:auto;
position:relative;
width:550px;
height:700px;
font-family: Tahoma, Geneva, sans-serif;
font-size: 14px;
font-style: italic;
line-height: 24px;
font-weight: bold;
color: #09C;
text-decoration: none;
-webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;
padding:10px;
border: 1px solid #999;
border: inset 1px solid #333;
-webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
}


input    {
width:375px;
display:block;
border: 1px solid #999;
height: 25px;
-webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
}

textarea#feedback {
width:375px;
height:150px;
}
textarea.message {
display:block;
}
input.button {
width:100px;
position:absolute;
right:20px;
bottom:20px;
background:#09C;
color:#fff;
font-family: Tahoma, Geneva, sans-serif;
height:30px;
-webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;
border: 1p solid #999;
}
input.button:hover {
background:#fff;
color:#09C;
}
textarea:focus, input:focus {
border: 1px solid #09C;
}
    
    
    
  </style>
  </head>
<!-- NAVBAR================================================== -->
 
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<c:url value="/index" />">Главная</a></li>
            <li class="dropdown">
                  <a href="<c:url value="/search" />" class="dropdown-toggle" data-toggle="dropdown">Поиск <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a  href="<c:url value="/search" />">Поиск по названию</a></li>
                    <li><a href="<c:url value="/search_type" />">Поиск по типу</a></li>
              
                  </ul>
                </li>
            <li><a href="<c:url value="/contact" />">Контакты</a></li>
            <c:if test="${username != null}"> 
            	<li><a href="<c:url value="/order" />">Как заказать</a></li>
            </c:if>
          </ul>    
            </div>
          </div>
        </div>
        <br><br>
        <br><br>
       	 	<div class="container"> 
	<div class="container"> 
<form:form class="form" method="POST" modelAttribute="physic" >
<div>
<h1>Contact Form :</h1>
	<label>
		<span>Ведите id:</span><form:input path="id"></form:input>
	</label>

	<label>
		<span>Ведите название:</span><form:input path="physicname"></form:input>
	</label>

	<label>
		<span>Ведите код:</span><form:input path="code"></form:input>
	</label>

	<label>
		<span>Ведите тип:</span><form:input path="type"></form:input>
	</label>

	<label>
		<span>Ведите изготовителя:</span><form:input path="maker"></form:input>
	</label>

	<label>
		<span>Ведите цену:</span><form:input path="price"></form:input>
	</label>

	<label>
		<span>Ведите ссылку на картинку:</span><form:input path="picture"></form:input>
	</label>

	<label>
		<span>Ведите описание</span><br><textarea id="feedback" name="description"></textarea>
		<form:input path="description"></form:input>
		<input value="Edit" type="submit" />
	</label>

	</div>
	</form:form>
</div>
     	
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.min.js"></script>
  </body>
</html>
