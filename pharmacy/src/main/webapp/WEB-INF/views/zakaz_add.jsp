<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; ">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/demo.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/style2.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/animate-custom.css" />
		<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
   <script type="text/javascript" src="jscript.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
<title>Оформление заказа</title>
</head>
<body>
 <table >
					<tr >
						<th>Найменование</th>
									
					</tr>
				<c:forEach items="${zakazList}" var="zakaz">	
					<tr>
						
						<td>${zakaz.name}</td>
						
						<td>${zakaz.tel}</td>
						<td>${zakaz.city}</td>

					</tr>
				</c:forEach>
					</table>
				<section>				
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                        <form:form method="post"  modelAttribute="zakaz">
                                <h1> Оформление заказа </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="L">Введите страну </label>
                                    <input id="usernamesignup" name="country" required="required" type="text" placeholder="Украина" maxlength="12" />
                                  
                                </p>  
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="E" > Введите город </label>
                                    <input id="usernamesignup" name="city" required="required" type="text" placeholder="Черкассы"/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="P">Ваш номер телефона </label>
                                    <input id="usernamesignup" path="tel" required="required" type="text" placeholder="+380978584887"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="P">Количество  </label>
                                    <input id="usernamesignup" path="number" required="required" type="text" placeholder="4"/>
                                </p>
                                <input id="usernamesignup" path="${user.id}" required="required" type="text" placeholder="4"/>
                                <p class="signin button"> 
									<input type="submit" value="Заказать"/> 
								</p>
								
                           </form:form>
                        </div>
                    </div>
                </div>  
            </section>
            
		
			
</body>
</html>