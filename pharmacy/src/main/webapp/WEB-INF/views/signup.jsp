<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
<title>Аптека-Онлайн</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8"> 
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/demo.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/style2.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/animate-custom.css" />
		<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
   <script type="text/javascript" src="jscript.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">

<script type="text/javascript">
	$("#submit").click(function () {
    $(".error").hide();
    var valueX = $("#passwordX").val();
    var valueY = $("#passwordY").val();
    if (valueX != valueY) {
        alert("Passwords do not match.");
    }
});
	</script>

</head>
<body >
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="<c:url value="/index" />">Главная</a></li>
            <c:if test="${empty username}" >
            	<li class="active"><a href="<c:url value="/signup" />">Регистрация</a></li>
            </c:if>
            <li class="dropdown">
                  <a href="<c:url value="/search" />" class="dropdown-toggle" data-toggle="dropdown">Поиск <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a  href="<c:url value="/search" />">Поиск по названию</a></li>
                    <li><a href="<c:url value="/search_type" />">Поиск по типу</a></li>
              
                  </ul>
                </li>
            <li><a href="<c:url value="/contact" />">Контакты</a></li>
            <c:if test="${username != null}"> 
            	<li><a href="<c:url value="/order" />">Как заказать</a></li>
            </c:if>
          </ul>    
             <c:choose>
      				<c:when test="${username == null}"> 
      				<div class="navbar-collapse collapse">
              			<form class="navbar-form navbar-right" role="form"  method="POST" action="<c:url value="/j_spring_security_check" />">
						<div class="form-group">
              				<input type="text" name="j_username" placeholder="Login" class="form-control" required>
           			 	</div>
           			 	<div class="form-group">
             				<input type="password" name="j_password" placeholder="Password" class="form-control" required>
           				 </div>
            			<button type="submit" class="btn btn-success">Sign in</button>
         				</form>        		
        			</div>
      					
      				</c:when>
					<c:when test="${username == 'admin'}">
					
					<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/admin" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	<li class="dropdown-header">Лекарства</li>
                    <li><a href="<c:url value="/physic_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/physic_change_delete"/>">Изменить/Удалить</a></li>
                    <li><a href="<c:url value="/physic_add"/>">Добавить</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Пользователи</li>
                    <li><a href="<c:url value="/user_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/user_change_delete"/>">Изменить/Удалить</a></li>
                    <li class="dropdown-header">Заказы</li>
                    <li><a href="<c:url value="/order_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/order_change"/>">Редактирование</a></li>
                    
                    <li><a href="<c:url value="/admin" />">admin</a></li>
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      				</c:when>
      			<c:otherwise>
      			<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span style="color:red">${username}</span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      			</c:otherwise>
			</c:choose>
            </div>
          </div>
        </div>
<br><br><br>
            <section>				
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                        <form:form method="post"  modelAttribute="user">
                                <h1> Регистрация </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="L">Ваш логин</label>
                                    <input id="usernamesignup" name="username" required="required" type="text" placeholder="mylogin" maxlength="12" />
                                  
                                </p>  
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="E" > Ваш email</label>
                                    <input id="emailsignup" name="email" required="required" type="email" placeholder="mylogin@mail.com"/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="P">Ваш пароль  </label>
                                    <form:password id="passwordsignup" path="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="P">Пожалуйста, подтвердите пароль  </label>
                                    <form:password id="passwordsignup_confirm" path="confirmPassword" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p class="signin button"> 
									<input type="submit" value="Регистрация"/> 
								</p>
								<div style="color:red"><form:errors path="*" cssClass="error" /></div>
                                <p class="change_link">  
									Вы уже зарегистрированы? 
									<a href="<c:url value="login.jsp" />" class="to_register"> Тогда войдите </a>
								</p>
                           </form:form>
                        </div>
                    </div>
                </div>  
            </section>
           
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.min.js"></script>
  
</body>
</html>
