<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
    <title>Контакты</title>

    <!-- Bootstrap core CSS -->
  	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/carousel.css" rel="stylesheet">
  
  </head>
<!-- NAVBAR================================================== -->
  <body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="<c:url value="/index" />">Главная</a></li>
            <c:if test="${empty username}" >
            	<li><a href="<c:url value="/signup" />">Регистрация</a></li>
            </c:if>
            <li class="dropdown">
                  <a href="<c:url value="/search" />" class="dropdown-toggle" data-toggle="dropdown">Поиск <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a  href="<c:url value="/search" />">Поиск по названию</a></li>
                    <li><a href="<c:url value="/search_type" />">Поиск по типу</a></li>
              
                  </ul>
                </li>
            <li class="active"><a href="<c:url value="/contact" />">Контакты</a></li>
            <c:if test="${username != null}"> 
            	<li><a href="<c:url value="/order" />">Как заказать</a></li>
            </c:if>
          </ul>    
             <c:choose>
      				<c:when test="${username == null}"> 
      				<div class="navbar-collapse collapse">
              			<form class="navbar-form navbar-right" role="form"  method="POST" action="<c:url value="/j_spring_security_check" />">
						<div class="form-group">
              				<input type="text" name="j_username" placeholder="Login" class="form-control" required>
           			 	</div>
           			 	<div class="form-group">
             				<input type="password" name="j_password" placeholder="Password" class="form-control" required>
           				 </div>
            			<button type="submit" class="btn btn-success">Sign in</button>
         				</form>        		
        			</div>
      					
      				</c:when>
					<c:when test="${username == 'admin'}">
					
					<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/admin" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	<li class="dropdown-header">Лекарства</li>
                    <li><a href="<c:url value="/physic_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/physic_change_delete"/>">Изменить/Удалить</a></li>
                    <li><a href="<c:url value="/physic_add"/>">Добавить</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Пользователи</li>
                    <li><a href="<c:url value="/user_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/user_change_delete"/>">Изменить/Удалить</a></li>
                    <li class="dropdown-header">Заказы</li>
                    <li><a href="<c:url value="/order_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/order_change"/>">Редактирование</a></li>
                      <li class="dropdown-header">Сообщения</li>
                    <li><a href="<c:url value="/contact_change_delete"/>">Просмотр/Удалить</a></li>
                    
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      				</c:when>
      			<c:otherwise>
      			<div class="navbar-collapse collapse">
				<div class="navbar-form navbar-right">
				<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/index" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	<li class="dropdown-header">Мои заказы</li>
                    <li><a href="<c:url value="/profile"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/order_add"/>">Оформить заказ</a></li>
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      			</c:otherwise>
			</c:choose>
            </div>
          </div>
        </div>
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img  src="${pageContext.request.contextPath}/resources/images/action/action1.jpg"  alt="First slide">
          
        </div>
        <div class="item">
          <img src="${pageContext.request.contextPath}/resources/images/action/action2.jpg" alt="Second slide">
          
        </div>
        <div class="item">
          <img src="${pageContext.request.contextPath}/resources/images/action/action3.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
		<div class="jumbotron">
			<c:if test="${username == null}"> 
    			<div class="alert alert-danger">
        			<strong>Внимание!</strong> &nbsp;Для заказа лекарств вам необходимо зарегистрироваться!
      			</div>
   		 	</c:if>
        	<h1>Обратная связь</h1>
       		<p>Адрес: 04213, г. Киев, ул. Приречная, 27-Е.<br>
			Как доехать: от м. «Минская» 559 маршрутным автобусом.<br>
			Телефон/факс: 044 531-38-72.<br>
			Электронная почта (исключительно для связи с офисом):а.info@e-apteka.com.ua.<br></p> 
        	<img src="${pageContext.request.contextPath}/resources/images/pharmacy.jpg">
          	<br><p style="color:black">
          		Предложения, жалобы, благодарности вопросы, оставляйте, пожалуйста, с помощью этой <a href="<c:url value="/contact_add"/>">формы </a>       	
          	</p></div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.min.js"></script>
  </body>
</html>
