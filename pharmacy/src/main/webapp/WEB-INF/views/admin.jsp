<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Pharmacy</title>
<style>

.form    {
background: -webkit-gradient(linear, bottom, left 175px, from(#CCCCCC), to(#EEEEEE));
background: -moz-linear-gradient(bottom, #CCCCCC, #EEEEEE 175px);
margin:auto;
position:relative;
width:550px;
height:450px;
font-family: Tahoma, Geneva, sans-serif;
font-size: 14px;
font-style: italic;
line-height: 24px;
font-weight: bold;
color: #09C;
text-decoration: none;
-webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;
padding:10px;
border: 1px solid #999;
border: inset 1px solid #333;
-webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
}


input    {
width:375px;
display:block;
border: 1px solid #999;
height: 25px;
-webkit-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
}

textarea#feedback {
width:375px;
height:150px;
}
textarea.message {
display:block;
}
input.button {
width:100px;
position:absolute;
right:20px;
bottom:20px;
background:#09C;
color:#fff;
font-family: Tahoma, Geneva, sans-serif;
height:30px;
-webkit-border-radius: 15px;
-moz-border-radius: 15px;
border-radius: 15px;
border: 1p solid #999;
}
input.button:hover {
background:#fff;
color:#09C;
}
textarea:focus, input:focus {
border: 1px solid #09C;
}



</style>
    <!-- Bootstrap core CSS -->
  	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
  </head>
<!-- NAVBAR================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse " role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li ><a href="<c:url value="/index" />">Главная</a></li>
                <li><a href="<c:url value="/signup" />">Регистрация</a></li>
                <li><a href="#contact">Контакты</a></li>
                <form class="navbar-form navbar-right">
            		<input type="text" class="form-control" placeholder="Search...">
          		</form>
          		
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Nav header</li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="<c:url value="/admin" />">admin</a></li>
                  </ul>
                </li>
              </ul>
                 <c:choose>
      				<c:when test="${username == null}"> 
      				<div class="navbar-collapse collapse">
              			<form class="navbar-form navbar-right" role="form"  method="POST" action="<c:url value="/j_spring_security_check" />">
           				<c:if test="${not empty param.error}">
							<font color="red"> Ошибка входа: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </font>
						</c:if>
						<div class="form-group">
              				<input type="text" name="j_username" placeholder="Login" class="form-control" required>
           			 	</div>
           			 	<div class="form-group">
             				<input type="password" name="j_password" placeholder="Password" class="form-control" required>
           				 </div>
            			<button type="submit" class="btn btn-success">Sign In</button>
         				</form>        		
        			</div>
      					</c:when>
      				
      				<c:otherwise>
      					<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
							Вы зашли как: <span style="color:red">${username}</span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      			</c:otherwise>
			</c:choose>
            </div>
          </div>
        </div>
<div> <c:forEach items="${physicList}" var="physic">
			<ul class="thumbnails">
 				<li class="col-xs-6 col-md-4">
    				<div class="thumbnail">
      					<img src="${physic.picture}" width="120" height="120">
      					<h3>${physic.physicname}</h3>
      					<p>${physic.description}</p>
    				</div>
 				</li>
			</ul>
		</c:forEach>
<table class="data" align="center" border="1">
		<tr>
			<th>Імя</th>
			<th>Код</th>
			<th>Виробник</th>
			<th>Ціна</th>
			<th>Картинка</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${physicList}" var="physic">
			<tr>
				<td>${physic.physicname}1</td>
				<td>${physic.code}2</td>
				<td>${physic.maker}3</td>
				<td>${physic.price}4</td>
				<td><img src="${physic.picture}" width="120" height="120"></td>
				
				
				
			<td><a href="delete/${physic.id}" onclick="return confirm('Вы уверены что хотите удать ${physic.physicname}?')">удалить</a>
			<a href="edit/${physic.id}">Edit</a>
			</td>
			</tr>
		</c:forEach>
	</table>
	 <div id="newSS">
        
   <form:form method="post" action="add" modelAttribute="physic">

	<table>
		<tr>
			<td><form:label path="physicname">
				physicname
			</form:label></td>
			<td><form:input path="physicname" /></td>
		</tr>
		<tr>
			<td><form:label path="code">
				code
			</form:label></td>
			<td><form:input path="code" /></td>
		</tr>
		<tr>
			<td><form:label path="maker">
				maker
			</form:label></td>
			<td><form:input path="maker" /></td>
		</tr>
		<tr>
			<td><form:label path="price">
				price
			</form:label></td>
			<td><form:input path="price" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit"
				value="add" /></td>
		</tr>
	</table>
</form:form>


    </div>
	<img src="${pageContext.request.contextPath}/resources/images/pharmacy/9470_large.jpg">
	<a href="<c:url value="/signup" />">
	регістер
</a><a href="<c:url value="/logout" />">
	выйти
</a>


<form class="form ">
<div>
<h1>Contact Form :</h1>
<label>
<span>Your name</span><input id="name" type="text" name="name" />
</label>

<label>
<span>Email Address</span><input id="email" type="text" name="email" />
</label>

<label>
<span>Subject</span><input id="subject" type="text" name="subject" />
</label>

<label>
<span>Message</span><textarea id="feedback" name="feedback"></textarea>
<input type="button" value="Submit Form" />
</label>

</div>
</form>
	<div class="container"> 
<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Редактирование</legend>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="Введите id">Введите id</label>
  <div class="controls">
    <input id="Введите id" name="Введите id" type="text" placeholder="" class="input-xlarge">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите название</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите код</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите тип</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите изготовителя</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите цену</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="textinput">Введите ссылку на картинку</label>
  <div class="controls">
    <input id="textinput" name="textinput" type="text" placeholder="" class="input-xlarge" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="control-group">
  <label class="control-label" for="textarea">Введите описание</label>
  <div class="controls">                     
    <textarea id="textarea" name="textarea"></textarea>
  </div>
</div>

</fieldset>
</form>
</div>

</div>
</div>

</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
