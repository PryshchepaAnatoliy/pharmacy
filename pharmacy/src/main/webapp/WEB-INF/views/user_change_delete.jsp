<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
    <title>Аптека-Онлайн</title>

    <!-- Bootstrap core CSS -->
  	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/carousel.css" rel="stylesheet">
  
  </head>
<!-- NAVBAR================================================== -->
 
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<c:url value="/index" />">Главная</a></li>
            <c:if test="${empty username}" >
            	<li><a href="<c:url value="/signup" />">Регистрация</a></li>
            </c:if>
            <li class="dropdown">
                  <a href="<c:url value="/search" />" class="dropdown-toggle" data-toggle="dropdown">Поиск <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a  href="<c:url value="/search" />">Поиск по названию</a></li>
                    <li><a href="<c:url value="/search_type" />">Поиск по типу</a></li>
              
                  </ul>
                </li>
            <li><a href="<c:url value="/contact" />">Контакты</a></li>
            <c:if test="${username != null}"> 
            	<li><a href="<c:url value="/order" />">Как заказать</a></li>
            </c:if>
          </ul>    
             <c:choose>
      				<c:when test="${username == null}"> 
      				<div class="navbar-collapse collapse">
              			<form class="navbar-form navbar-right" role="form"  method="POST" action="<c:url value="/j_spring_security_check" />">
						<div class="form-group">
              				<input type="text" name="j_username" placeholder="Login" class="form-control" required>
           			 	</div>
           			 	<div class="form-group">
             				<input type="password" name="j_password" placeholder="Password" class="form-control" required>
           				 </div>
            			<button type="submit" class="btn btn-success">Sign in</button>
         				</form>        		
        			</div>
      					
      				</c:when>
					<c:when test="${username == 'admin'}">
					
					<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/admin" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	<li class="dropdown-header">Лекарства</li>
                    <li><a href="<c:url value="/physic_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/physic_change_delete"/>">Изменить/Удалить</a></li>
                    <li><a href="<c:url value="/physic_add"/>">Добавить</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Пользователи</li>
                    <li><a href="<c:url value="/user_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/user_change_delete"/>">Изменить/Удалить</a></li>
                    <li class="dropdown-header">Заказы</li>
                    <li><a href="<c:url value="/order_review"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/order_change"/>">Редактирование</a></li>
                      <li class="dropdown-header">Сообщения</li>
                    <li><a href="<c:url value="/contact_change_delete"/>">Просмотр/Удалить</a></li>
                    
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      				</c:when>
      			<c:otherwise>
      			<div class="navbar-collapse collapse">
				<div class="navbar-form navbar-right">
				<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/index" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	<li class="dropdown-header">Мои заказы</li>
                    <li><a href="<c:url value="/profile"/>">Просмотр</a></li>
                    <li><a href="<c:url value="/order_add"/>">Оформить заказ</a></li>
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      			</c:otherwise>
			</c:choose>
            </div>
          </div>
        </div>
        <br><br>
        <br><br>
       	 	<div class="container"> 
        
        		<table class="table" border=1  style="border: 1px solid #DCDCDC; text-align:center;">
					<tr >
						<th>Никнейм</th>
						<th>Пароль</th>
						<th>Имейл</th>
						<th>Розбанен/Забанен</th>
						<th>Статус</th>	
						<th>&nbsp;</th>
										
					</tr>
				<c:forEach items="${userList}" var="user">	
					<tr>
						<td>${user.username}</td>
						<td>${user.password}</td>
						<td>${user.email}</td>
						<td>${user.enabled} <a  href="setBan/${user.id}" ><img src="${pageContext.request.contextPath}/resources/images/user/ban/ban.png" ></a>/
					<a  href="setUnban/${user.id}" ><img src="${pageContext.request.contextPath}/resources/images/user/ban/unban.png" ></a>
					</td>
						<td>${user.role}</td>
						<td>
					<a  href="setUser/${user.id}" ><img src="${pageContext.request.contextPath}/resources/images/user/user.png" ></a>/
					<a  href="setAdmin/${user.id}" ><img src="${pageContext.request.contextPath}/resources/images/user/admin.png" ></a>/
					<a  href="delete1/${user.id}" onclick="return confirm('Вы уверены что хотите удать ${user.username}?')" ><img src="${pageContext.request.contextPath}/resources/images/user/user_adm.png" ></a>
					
						</td>
					</tr>
				</c:forEach>
				</table>
   		 	</div>
 <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/docs.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.min.js"></script>
  </body>
</html>
