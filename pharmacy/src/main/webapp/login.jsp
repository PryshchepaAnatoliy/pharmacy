<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
  <head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width" charset="utf-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/demo.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/style2.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/animate-custom.css" />
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"	rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js" type="text/javascript"></script>
   


 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/logo.ico" type="image/x-icon">
    <title>Pharmacy</title>

    <!-- Bootstrap core CSS -->

  </head>
<!-- NAVBAR================================================== -->
  <body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<c:url value="/index" />">Главная</a></li>
            <c:if test="${empty username}" >
            	<li><a href="<c:url value="/signup" />">Регистрация</a></li>
            </c:if>
            <li><a href="<c:url value="/search" />">Поиск</a></li>
            <li><a href="<c:url value="/contact" />">Контакты</a></li>
            <c:if test="${username != null}"> 
            	<li><a href="<c:url value="/contact" />">Как заказать</a></li>
            </c:if>
          </ul>    
             <c:choose>
      				<c:when test="${username == null}"> 
      					
      				</c:when>
					<c:when test="${username == 'admin'}">
					
					<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span class="dropdown">
                  <a href="<c:url value="/admin" />" class="dropdown-toggle" data-toggle="dropdown">${username} <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                 	 <li class="dropdown-header">Лекарства</li>
                    <li><a href="#"></a></li>
                    <li><a href="#">Просмотр</a></li>
                    <li><a href="#">Редактирование</a></li>
                    <li><a href="#">Удаление</a></li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Пользователи</li>
                    <li><a href="#">Просмотр</a></li>
                    <li><a href="#">Редактирование</a></li>
                    <li><a href="#">Удаление</a></li>
                    <li><a href="<c:url value="/admin" />">admin</a></li>
                  </ul>
                </span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      				</c:when>
      			<c:otherwise>
      			<div class="navbar-collapse collapse">
						<div class="navbar-form navbar-right">
						<div class="form-group">
						Вы зашли как: <span style="color:red">${username}</span>
            			</div>
            			<a class="btn btn-success" href="<c:url value="/logout" />">
							Bыйти
					</a></div>
      				</div>
      			</c:otherwise>
			</c:choose>
            </div>
          </div>
        </div>
<br><br><br>
 <section>				
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                        <form   method="POST" action="<c:url value="/j_spring_security_check" />">                          
                                <h1>Войти</h1> 
                                <p> 
                                    <label for="username" class="uname" data-icon="L" > Ваш логин </label>
                                    <input id="username"  name="j_username" required="required" type="text" placeholder="mylogin"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="P"> Ваш пароль  </label>
                                    <input id="password" name="j_password" required="required" type="password" placeholder=" например eg. X8df!90EO" /> 
                                <c:if test="${not empty param.error}">
										<font color="red"> Ошибка входа
										: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </font>
									</c:if></p>
                                <p class="keeplogin"> 
									<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
									<label for="loginkeeping">Запомнить меня</label> 
									
								</p>
                                <p class="login button"> 
                                    <input type="submit" value="Войти" /> 
								</p>
                                 <p class="change_link">
									Вы еще не с нами? 
									<a href="<c:url value="/signup" />" class="to_register">Присоединиться</a>
								</p>
                            </form>
                        </div>
                    </div>
                </div>  
            </section>


   </body>
</html>
