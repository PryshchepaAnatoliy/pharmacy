package com.chpt.pharmacy.web;

import java.security.Principal;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.chpt.pharmacy.entity.Feed;
import com.chpt.pharmacy.entity.Physic;
import com.chpt.pharmacy.entity.User;
import com.chpt.pharmacy.entity.Zakaz;
import com.chpt.pharmacy.service.FeedService;
import com.chpt.pharmacy.service.PhysicService;
import com.chpt.pharmacy.service.UserService;
import com.chpt.pharmacy.service.ZakazService;


@Controller

public class PhysicController {


    @Autowired
    private PhysicService physicService;
    
    @Autowired
	private UserService userService;
    
    @Autowired
  	private FeedService feedService;
    
    @Autowired
  	private ZakazService zakazService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Map<String, Object> map, Principal principal,
    		@RequestParam(value = "name",required = false) String name,
    		Model model) {
	    	model.addAttribute("find", physicService.getByName(name));
	       
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }

        return "search";
    }
    
   
    @RequestMapping(value ="/search" ,method = RequestMethod.POST)
    public String listphysic1(Map<String, Object> map,@RequestParam(value = "name",required = false) String name,
    		Model model,Principal principal) {
        model.addAttribute("find", physicService.getByName(name));
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }

        return "search";
    }
    
    @RequestMapping(value = "/search_type", method = RequestMethod.GET)
    public String searchType(Map<String, Object> map, Principal principal,
    		@RequestParam(value = "type",required = false) String type,
    		Model model) {
	    	model.addAttribute("findType", physicService.getByType(type));
	       
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }

        return "search_type";
    }
    
    @RequestMapping(value ="/search_type" ,method = RequestMethod.POST)
    public String searchType(Map<String, Object> map,@RequestParam(value = "type",required = false) String type,
    		Model model,Principal principal) {
        model.addAttribute("findType", physicService.getByType(type));
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }

        return "search_type";
    }
   
    @RequestMapping("/admin")
    public String listadmin(Map<String, Object> map,Principal principal) {

        map.put("physic", new Physic());
        map.put("physicList", physicService.listPhysic());
        
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "admin";
    }
    
    @RequestMapping("/admin")
    public String edit(Map<String, Object> map,Principal principal) {

        map.put("physic", new Physic());
        map.put("physicList", physicService.listPhysic());
        
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "admin";
    }
    
    @RequestMapping("/physic_review")
    public String physicReview(Map<String, Object> map,Principal principal) {

        map.put("physic", new Physic());
        map.put("physicList", physicService.listPhysic());
        
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "physic_review";
    }
    
    @RequestMapping("/physic_change_delete")
    public String physicCangeDel(Map<String, Object> map,Principal principal) {

        map.put("physic", new Physic());
        map.put("physicList", physicService.listPhysic());

        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "physic_change_delete";
    }
    @RequestMapping("/physic_add")
    public String physicAdd(Map<String, Object> map,Principal principal) {
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "physic_add";
    }
    @RequestMapping(value="/contact", method = RequestMethod.POST)
    public String feed(@ModelAttribute("feed") Feed feed, Map<String, Object> map,Principal principal) {
    	feedService.addFeed(feed);
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "redirect:/index";
    }
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String feed(Map<String, Object> map, Model model,Principal principal) {	
		model.addAttribute("feed", new Feed());
		if (principal != null){
        	map.put("username", principal.getName());
        }
		return "contact";
	}
    
    @RequestMapping(value="/contact_add", method = RequestMethod.POST)
    public String addfeed(@ModelAttribute("feed") Feed feed, Map<String, Object> map,Principal principal) {
    	feedService.addFeed(feed);
        if (principal != null){
        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "redirect:/index";
    }
    @RequestMapping(value = "/contact_add", method = RequestMethod.GET)
	public String addfeed(Map<String, Object> map, Model model,Principal principal) {	
		model.addAttribute("feed", new Feed());
		if (principal != null){
        	map.put("username", principal.getName());
        }
		return "/contact_add";
	}

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPhysic(@ModelAttribute("physic") Physic physic,
            BindingResult result) {
    	physicService.addPhysic(physic);
        return "redirect:/physic_review";
    }
    
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
        public ModelAndView editPhysicPage(@PathVariable Integer id) {
            ModelAndView modelAndView = new ModelAndView("edit-team-form");
            Physic physic = physicService.getPhysic(id);
            modelAndView.addObject("physic",physic);
            return modelAndView;
        }

        @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
        public ModelAndView edditingTeam(@ModelAttribute Physic physic, @PathVariable Integer id) {
            ModelAndView modelAndView = new ModelAndView("redirect:/physic_review");
            physicService.updatePhysic(physic);
            return modelAndView;
        }



    @RequestMapping("/delete/{physicId}")
    public String deletePhysic(@PathVariable("physicId") Integer physicId) {
    	physicService.removePhysic(physicId);
        return "redirect:/physic_change_delete";
    }
  
    @RequestMapping("/delete1/{userId}")
    public String deleteUser(@PathVariable("userId") Integer userId) {
    	userService.removeUser(userId);
        return "redirect:/user_review";
    }
    @RequestMapping("/deletefeed/{feedId}")
    public String deleteFeed(@PathVariable("feedId") Integer feedId) {
    	feedService.removeFeed(feedId);
        return "redirect:/contact_change_delete";
    }
    

    @RequestMapping(value = "/logout")
    public String logout() {
        return "index";
    }
    
	
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user,
			BindingResult result) {
    
    	if(result.hasErrors()) {
    		return "signup";
    	}
		userService.addUser(user);
		return "signup";
		
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String addUser(Map<String, Object> map, Model model,Principal principal) {
		model.addAttribute("user", new User());
		if (principal != null){
        	map.put("username", principal.getName());
        }
		return "signup";
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile(Map<String, Object> map,Model model,Principal principal) {
		
		model.addAttribute("userZakaz", userService.getZakaz(userService.getIdByUsername(principal.getName())));
		//System.out.println(userService.getZakaz(3).get(0));
		if (principal != null){
        	map.put("username", principal.getName());
        }
		return "profile";
	}

	   @RequestMapping(value = "/index", method = RequestMethod.GET)
	    public String listphysic2(Map<String, Object> map, Principal principal,
	    		@RequestParam(value = "name",required = false) String name,
	    		Model model) {

	     //  SecurityContextHolder.getContext().getAuthentication().getName(); 
	      //  System.out.println(name);
		  // if(name != null) {
		       
		       
		       // System.out.println(physicService.getByName(name).getId());
		 //  } else {
			   map.put("physic", new Physic());
		       map.put("physicList", physicService.listPhysic());
		      
		     
		    	//   model.addAttribute("find", physicService.getByName(name));
		       
		        if (principal != null){
		        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
		        }
		   
	        return "index";
	    }
	   
	   @RequestMapping(value = "/index", method = RequestMethod.POST)
	    public String listphysicPOST(Map<String, Object> map,Model model, @RequestParam(value = "name", 
	    	required = true) String name, Principal principal) {
			model.addAttribute("find", physicService.getByName(name));
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }
	        return "redirect:/index1";
	        
	    }
	   @RequestMapping("/user_review")
	    public String userReview(Map<String, Object> map,Principal principal) {

	        map.put("user", new User());
	        map.put("userList", userService.listUser());
	        
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }
	        return "user_review";
	    }
	   
	   @RequestMapping("/user_change_delete")
	    public String userChangeDel(Map<String, Object> map,Principal principal,User user) {
		   
		   
	        map.put("user", new User());
	        map.put("userList", userService.listUser());
	        
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }
	        return "user_change_delete";
	    }

	    @RequestMapping(value="/setAdmin/{userId}",method = RequestMethod.GET)
	    public String setAdm(@PathVariable("userId") Integer userId) {
	    	userService.setRoleAdmin(userId);
	        return "redirect:/user_change_delete";
	    }
	    
	    @RequestMapping(value="/setUser/{userId}",method = RequestMethod.GET)
	    public String setUser(@PathVariable("userId") Integer userId) {
	    	userService.setRoleUser(userId);
	        return "redirect:/user_change_delete";
	    }
	    
	    @RequestMapping(value="/setBan/{userId}",method = RequestMethod.GET)
	    public String setBan(@PathVariable("userId") Integer id) {
	    	userService.setBan(id);
	        return "redirect:/user_change_delete";
	    }
	    
	    @RequestMapping(value="/setUnban/{userId}",method = RequestMethod.GET)
	    public String setUnban(@PathVariable("userId") Integer id) {
	    	userService.setUnban(id);
	        return "redirect:/user_change_delete";
	    }
	    
	   @RequestMapping(value = "/order", method = RequestMethod.GET)
	    public String order(Map<String, Object> map, Principal principal,
	    		@RequestParam(value = "name",required = false) String name,
	    		Model model) {
			   	map.put("physic", new Physic());
			   	map.put("physicList", physicService.listPhysic());
		        if (principal != null){
		        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
		}
		   
	        return "order";
	    }
	   
	   @RequestMapping(value = "/order", method = RequestMethod.POST)
	    public String order(Map<String, Object> map,Model model, @RequestParam(value = "name", 
	    	required = true) String name, Principal principal) {
	       
	        if (principal != null){
	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
	        }
	        return "redirect:/order";
	   }
//	    }
	   @RequestMapping(value = "/zakaz", method = RequestMethod.GET)
	 		public String zakaz(Map<String, Object> map, Model model,Principal principal) {
	 	    	map.put("zakaz", new Zakaz());
	 		    map.put("zakazList",zakazService.listZakaz());
	 			//model.addAttribute("zakaz", new Zakaz());
	 			if (principal != null){
	 	        	map.put("username", principal.getName());
	 	        }
	 			return "zakaz";
	 		}
//	   
//	   @RequestMapping(value="/zakaz_add", method = RequestMethod.GET)
//	    public String addorder(@ModelAttribute("zakaz") Zakaz zakaz,Model model, Map<String, Object> map,Principal principal) {
//	    	
//		   model.addAttribute("zakaz", new Zakaz());
////	        map.put("zakaz", new Zakaz());
////	        map.put("zakazList", (zakazService).listZakaz());
//	        
//	        if (principal != null){
//	        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
//	        }
//	        return "zakaz_add";
//	    }
	   
//	    @RequestMapping(value = "/zakaz_add", method = RequestMethod.POST)
//	    public String addZakaz(@ModelAttribute("zakaz") Zakaz zakaz) {
//	    	//if(result.hasErrors()) {
//	    	//	return "zakaz_add";
//	    	//}
//			zakazService.addZakaz(zakaz);
//			return "signup";
//			
//		}
//	    @RequestMapping(value = "/zakaz_add", method = RequestMethod.GET)
//		public String addZakaz(Map<String, Object> map, Model model,Principal principal) {
//			model.addAttribute("zakaz", new Zakaz());
//			if (principal != null){
//	        	map.put("username", principal.getName());
//	        }
//			return "zakaz_add";
//		}
//	   @RequestMapping(value="/user", method = RequestMethod.GET)
//	    public String listContacts(Model model, @RequestParam(value = "id", required = true) Integer id) {
//	    	model.addAttribute("user", userService.getById(id));
//	    	 
//	        return "user";
//	    }
	  
		 @RequestMapping(value = "/zakaz_add", method = RequestMethod.POST)
		public String addZakaz(@Valid @ModelAttribute("zakaz") Zakaz zakaz,
				BindingResult result) {
		
			if(result.hasErrors()) {
				return "signup";
			}
			zakazService.addZakaz(zakaz);
			return "zakaz_add";
			
		}
		
		@RequestMapping(value = "/zakaz_add", method = RequestMethod.GET)
		public String addZakaz(Map<String, Object> map, Model model,Principal principal) {
			model.addAttribute("zakaz", new Zakaz());
			if (principal != null){
		    	map.put("username", principal.getName());
		    }
			return "zakaz_add";
		}
//		   @RequestMapping("/contact_change_delete")
//		    public String contactCangeDel(Map<String, Object> map,Principal principal) {
//
//		        map.put("feed", new Feed());
//		        map.put("feedList", feedService.listFeed());
//
//		        if (principal != null){
//		        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
//		        }
//		        return "contact_change_delete";
//		    }
		   @RequestMapping(value = "/contact_change_delete", method = RequestMethod.GET)
		    public String contactchange(Map<String, Object> map, Principal principal,
		    		Model model) {
				   map.put("feed", new Feed());
			       map.put("feedList", feedService.listFeed());
			        if (principal != null){
			        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
			        }
			   
		        return "contact_change_delete";
		    }
		   
		   @RequestMapping(value = "/contact_change_delete", method = RequestMethod.POST)
		    public String contactchange(Map<String, Object> map,Model model, Principal principal) {
		        if (principal != null){
		        	map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
		        }
		        return "redirect:/contact_change_delete";
		   }
}