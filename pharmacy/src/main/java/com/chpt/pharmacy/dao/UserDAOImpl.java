package com.chpt.pharmacy.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;






import com.chpt.pharmacy.entity.Physic;
import com.chpt.pharmacy.entity.User;
import com.chpt.pharmacy.entity.Zakaz;


@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
	private SessionFactory sessionFactory;
	
	public void addUser(User user){
		sessionFactory.getCurrentSession().save(user);
		
	}
    
    @SuppressWarnings("unchecked")
    public List<User> listUser() {

        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    public void removeUser(Integer id) {
    	User user = (User) sessionFactory.getCurrentSession().load(
    			User.class, id);
        if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
        }

    } 
    public void setRoleAdmin(Integer id) {

    	User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
    	user.setRole("ROLE_ADMIN");
    	sessionFactory.getCurrentSession().update(user);
    }
    public void setRoleUser(Integer id) {

    	User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
    	user.setRole("ROLE_USER");
    	sessionFactory.getCurrentSession().update(user);
    }
    
    public void setBan(Integer id) {

    	User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
    	user.setEnabled(false);
    	sessionFactory.getCurrentSession().update(user);
    }
    public void setUnban(Integer id) {

    	User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
    	user.setEnabled(true);
    	sessionFactory.getCurrentSession().update(user);
    }
    

	public List<Zakaz> getZakaz(Integer id) {
		
		User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
		return user.getZakaz();
	}

	public Integer getIdByUsername(String username) {
		
//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Physic.class);
//		criteria.add(Restrictions.eq("physicname", username)).list();
		User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("username", username)).uniqueResult();
		
		return user.getId();
	}
		
}