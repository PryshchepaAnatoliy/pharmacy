package com.chpt.pharmacy.dao;

import java.util.List;

import com.chpt.pharmacy.entity.Zakaz;

public interface ZakazDAO {

    public void addZakaz(Zakaz zakaz);
    

    public List<Zakaz> listZakaz();

    public void removeZakaz(Integer id);
}