package com.chpt.pharmacy.dao;



import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.chpt.pharmacy.entity.Physic;

@Repository
public class PhysicDAOImpl implements PhysicDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addPhysic(Physic physic) {
        sessionFactory.getCurrentSession().save(physic);
    }
    public Physic getPhysic(int id) {
    	return (Physic) sessionFactory.getCurrentSession().get(Physic.class, id);
    	
    }
    public void updatePhysic(Physic physic) {
    	Session session = sessionFactory.getCurrentSession();
    		Physic physicToUpdate = (Physic) session.get(Physic.class, physic.getId());
    		physicToUpdate.setId(physic.getId());
    		physicToUpdate.setPhysicname(physic.getPhysicname());
    		physicToUpdate.setCode(physic.getCode());  
    		physicToUpdate.setType(physic.getType()); 
    		physicToUpdate.setMaker(physic.getMaker());  
    		physicToUpdate.setPrice(physic.getPrice());  
    		physicToUpdate.setPicture(physic.getPicture()); 
    		physicToUpdate.setDescription(physic.getDescription()); 
    		session.save(physicToUpdate);
    }
    
    @SuppressWarnings("unchecked")
    public List<Physic> listPhysic() {

        return sessionFactory.getCurrentSession().createQuery("from Physic").list();
    }

    public void removePhysic(Integer id) {
    	Physic physic = (Physic) sessionFactory.getCurrentSession().load(
    			Physic.class, id);
        if (null != physic) {
            sessionFactory.getCurrentSession().delete(physic);
        }

    }
	@Override
	public List<Physic> getByName(String name) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Physic.class);
		return criteria.add(Restrictions.eq("physicname", name)).list();
	}	
	
	public List<Physic> getByType(String type) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Physic.class);
		return criteria.add(Restrictions.eq("type", type)).list();
	}	
}