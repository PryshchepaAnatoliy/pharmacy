package com.chpt.pharmacy.dao;

import java.util.List;

import com.chpt.pharmacy.entity.Physic;

public interface PhysicDAO {

    public void addPhysic(Physic physic);
    
    public void updatePhysic(Physic physic);

    public Physic getPhysic(int id);
    
    public List<Physic> getByName(String name);
    
    public List<Physic> getByType(String type);

    public List<Physic> listPhysic();

    public void removePhysic(Integer id);
}