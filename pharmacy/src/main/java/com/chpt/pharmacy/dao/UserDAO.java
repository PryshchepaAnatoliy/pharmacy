package com.chpt.pharmacy.dao;

import java.util.ArrayList;
import java.util.List;

import com.chpt.pharmacy.entity.User;
import com.chpt.pharmacy.entity.Zakaz;


public interface UserDAO {
	
	public void addUser(User user);
    public List<User> listUser();
    public void removeUser(Integer id);
    public void setRoleAdmin(Integer id);
    public void setRoleUser(Integer id);
    public void setBan(Integer id);
    public void setUnban(Integer id);
    public List <Zakaz> getZakaz(Integer id);
    public Integer getIdByUsername(String username);
}
