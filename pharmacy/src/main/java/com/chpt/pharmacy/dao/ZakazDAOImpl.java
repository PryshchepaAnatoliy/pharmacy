package com.chpt.pharmacy.dao;



import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.chpt.pharmacy.entity.Zakaz;

@Repository
public class ZakazDAOImpl implements ZakazDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addZakaz(Zakaz zakaz) {
        sessionFactory.getCurrentSession().save(zakaz);
    }

    
    @SuppressWarnings("unchecked")
    public List<Zakaz> listZakaz() {

        return sessionFactory.getCurrentSession().createQuery("from Zakaz").list();
    }

    public void removeZakaz(Integer id) {
    	Zakaz zakaz = (Zakaz) sessionFactory.getCurrentSession().load(
    			Zakaz.class, id);
        if (null != zakaz) {
            sessionFactory.getCurrentSession().delete(zakaz);
        }

    }
	
}