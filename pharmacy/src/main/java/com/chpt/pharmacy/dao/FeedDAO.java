package com.chpt.pharmacy.dao;

import java.util.List;

import com.chpt.pharmacy.entity.Feed;

public interface FeedDAO {

    public void addFeed(Feed feed);
    

    public List<Feed> listFeed();

    public void removeFeed(Integer id);
}