package com.chpt.pharmacy.dao;



import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.chpt.pharmacy.entity.Feed;

@Repository
public class FeedDAOImpl implements FeedDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addFeed(Feed feed) {
        sessionFactory.getCurrentSession().save(feed);
    }

    
    @SuppressWarnings("unchecked")
    public List<Feed> listFeed() {

        return sessionFactory.getCurrentSession().createQuery("from Feed").list();
    }

    public void removeFeed(Integer id) {
    	Feed feed = (Feed) sessionFactory.getCurrentSession().load(
    			Feed.class, id);
        if (null != feed) {
            sessionFactory.getCurrentSession().delete(feed);
        }

    }
	
}