package com.chpt.pharmacy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chpt.pharmacy.dao.ZakazDAO;
import com.chpt.pharmacy.entity.Zakaz;

@Service
public class ZakazServiceImpl implements ZakazService  {

	@Autowired
    private ZakazDAO zakazDAO;
 
    @Transactional
    public void addZakaz(Zakaz zakaz) {
    	zakazDAO.addZakaz(zakaz);
    }
    
    @Transactional
    public List<Zakaz> listZakaz() {
        return zakazDAO.listZakaz();
    }
 
    @Transactional
    public void removeZakaz(Integer id) {
    	zakazDAO.removeZakaz(id);
    }
    

      
}
	


