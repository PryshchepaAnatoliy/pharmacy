package com.chpt.pharmacy.service;


import java.util.List;

import com.chpt.pharmacy.entity.Feed;


	public interface FeedService {

	    public void addFeed(Feed feed);
 
	    public List<Feed> listFeed();

	    public void removeFeed(Integer id);


	}
	
