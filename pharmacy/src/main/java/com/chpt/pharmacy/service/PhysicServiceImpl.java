package com.chpt.pharmacy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chpt.pharmacy.dao.PhysicDAO;
import com.chpt.pharmacy.entity.Physic;

@Service
public class PhysicServiceImpl implements PhysicService  {

	@Autowired
    private PhysicDAO physicDAO;
 
    @Transactional
    public void addPhysic(Physic physic) {
    	physicDAO.addPhysic(physic);
    }
    
    @Transactional
    public void updatePhysic(Physic physic) {
    	physicDAO.updatePhysic(physic);
    }
    
    @Transactional
    public Physic getPhysic(int id) {
    	return physicDAO.getPhysic(id);
    }
    
    @Transactional
    public List<Physic> listPhysic() {
        return physicDAO.listPhysic();
    }
 
    @Transactional
    public void removePhysic(Integer id) {
    	physicDAO.removePhysic(id);
    }
    
    @Transactional
	@Override
	public List<Physic> getByName(String name) {
		return physicDAO.getByName(name);
	}
    
    @Transactional
	@Override
	public List<Physic> getByType(String type) {
		return physicDAO.getByType(type);
	}

      
}
	


