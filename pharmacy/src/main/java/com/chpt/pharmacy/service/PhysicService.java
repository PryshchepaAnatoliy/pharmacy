package com.chpt.pharmacy.service;


import java.util.List;

import com.chpt.pharmacy.entity.Physic;


	public interface PhysicService {

	    public void addPhysic(Physic physic);
	    public void updatePhysic(Physic physic);
	    public Physic getPhysic(int id);    
	    public List<Physic> listPhysic();
	    public void removePhysic(Integer id);
	    public List<Physic> getByName(String name);
	    public List<Physic> getByType(String type);

	}
	
