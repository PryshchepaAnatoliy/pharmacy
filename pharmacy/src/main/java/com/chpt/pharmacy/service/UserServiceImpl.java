package com.chpt.pharmacy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;






import com.chpt.pharmacy.dao.UserDAO;
import com.chpt.pharmacy.entity.User;
import com.chpt.pharmacy.entity.Zakaz;

@Service
public class UserServiceImpl implements UserService  {

	@Autowired
    private UserDAO userDAO;
	
	
	@Transactional  
	public void addUser(User user){
		user.setRole("ROLE_USER");
		user.setEnabled(true);
		userDAO.addUser(user);
	}
	
	
    
    @Transactional
    public List<User> listUser() {
        return userDAO.listUser();
    }
 
    @Transactional
    public void removeUser(Integer id) {
    	userDAO.removeUser(id);
    }
    
    @Transactional
    public void setUnban(Integer id) {
    	userDAO.setUnban(id);
    }
    
    @Transactional
    public void setBan(Integer id) {
    	userDAO.setBan(id);
    }
    
    @Transactional
    public void setRoleAdmin(Integer id) {
    	userDAO.setRoleAdmin(id);
    }
    
    @Transactional
    public void setRoleUser(Integer id) {
    	userDAO.setRoleUser(id);
    }
    
    @Transactional
	public List<Zakaz> getZakaz(Integer id) {
		
		return userDAO.getZakaz(id);
	}

    @Transactional
	public Integer getIdByUsername(String username) {
		return userDAO.getIdByUsername(username);
	}
      
}
	


