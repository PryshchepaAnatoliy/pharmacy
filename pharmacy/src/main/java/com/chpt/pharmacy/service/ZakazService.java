package com.chpt.pharmacy.service;


import java.util.List;

import com.chpt.pharmacy.entity.Zakaz;


	public interface ZakazService {

	    public void addZakaz(Zakaz zakaz);
 
	    public List<Zakaz> listZakaz();

	    public void removeZakaz(Integer id);


	}
	
