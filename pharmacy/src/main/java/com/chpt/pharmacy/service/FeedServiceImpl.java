package com.chpt.pharmacy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chpt.pharmacy.dao.FeedDAO;
import com.chpt.pharmacy.entity.Feed;

@Service
public class FeedServiceImpl implements FeedService  {

	@Autowired
    private FeedDAO feedDAO;
 
    @Transactional
    public void addFeed(Feed feed) {
    	feedDAO.addFeed(feed);
    }
    
   
    
    @Transactional
    public List<Feed> listFeed() {
        return feedDAO.listFeed();
    }
 
    @Transactional
    public void removeFeed(Integer id) {
    	feedDAO.removeFeed(id);
    }
    

      
}
	


