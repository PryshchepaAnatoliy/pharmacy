package com.chpt.pharmacy.entity;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;




@Entity
@Table(name = "USER")
public class User {

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer id;
	
	
	@Column(name = "USERNAME",unique = true)
	@Size(min=4, max=20,
	message="��� ������ ���� �� 4 �� 20 ��������")
	@Pattern(regexp="^[a-zA-Z0-9]+$",
	message="��� ������ �������� �� ���������� ��������� �� ����� ��������")
	private String username;

	@Column(name = "PASSWORD")
	@Size(min=6, max=20,
	message="������ ������ ���� �� ������ 6 ��������")
	private String password;

	@Column(name = "ENABLED")
	private Boolean enabled;

	@Column(name = "ROLE")
	private String role;
	
	@Column(name = "CONFIRMPASSWORD")
	@Size(min=6, max=20,
	message="������ ������ ���� �� ������ 6 ��������")
	private String confirmPassword;
	
	@Column(name = "EMAIL")
	@Pattern(regexp="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}",
	message="�� ��������� ������ email �����.")
	private String email;
	
	//@OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
	@OneToMany(mappedBy = "user",fetch=FetchType.EAGER)
	private List<Zakaz> zakaz;


	public List<Zakaz> getZakaz() {
		return zakaz;
	}

	public void setZakaz(List<Zakaz> zakaz) {
		this.zakaz = zakaz;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}


}

	