package com.chpt.pharmacy.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



		@Entity
		@Table(name = "ZAKAZ")
public class Zakaz {
	
		@Id
		@Column(name = "ID")
		@GeneratedValue
		private Integer id;

		@Column(name = "NAME")
		private String name;
		
		@Column(name = "COUNTRY")
		private String country;
		
		@Column(name = "CITY")
		private String city;
		
		@Column(name = "TEL")
		private String tel;
		
//		@Column(name = "PHYSIC_ID")
//		private Integer physic_id;
		
		
		//@ManyToOne(fetch = FetchType.LAZY)
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name ="user_id")
		private User user;
		
		@Column(name = "NUMBER")
		private Integer number;
		
		@Column(name = "PRICE")
		private Integer price;
		
		public Integer getPrice() {
			return price;
		}

		public void setPrice(Integer price) {
			this.price = price;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getTel() {
			return tel;
		}

		public void setTel(String tel) {
			this.tel = tel;
		}

//		public Integer getPhysic_id() {
//			return physic_id;
//		}
//
//		public void setPhysic_id(Integer physic_id) {
//			this.physic_id = physic_id;
//		}


		public Integer getNumber() {
			return number;
		}

		public void setNumber(Integer number) {
			this.number = number;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		
		
		
}
