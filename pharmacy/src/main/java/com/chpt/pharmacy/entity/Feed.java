package com.chpt.pharmacy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

		@Entity
		@Table(name = "FEED")
public class Feed {
	
		@Id
		@Column(name = "ID")
		@GeneratedValue
		private Integer id;

		@Column(name = "NAME")
		private String name;


		@Column(name = "EMAIL")
		private String email;

		@Column(name = "PHONE")
		private String phone;
		
		@Column(name = "MESSAGE")
		private String message;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
		
		
		
}
